import 'dart:math';

import 'package:flutter/material.dart';

double getScreenHeightExcludeSafeArea(BuildContext context) {
  final double height =  MediaQuery.of(context).size.height;
  final EdgeInsets padding = MediaQuery.of(context).padding;
  return height - padding.top - padding.bottom;
}
double getScreenWidthExcludeSafeArea(BuildContext context) {
  final double width =  MediaQuery.of(context).size.width;
  final EdgeInsets padding = MediaQuery.of(context).padding;
  return width - padding.left - padding.right;
}
String getRandomString(int length) {
  const chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
  final random = Random();
  final buffer = StringBuffer();

  for (var i = 0; i < length; i++) {
    buffer.write(chars[random.nextInt(chars.length)]);
  }

  return buffer.toString();
}