import 'package:awesome_notifications/awesome_notifications.dart';

Future<bool> callNotify() async{
  String localTimeZone = await AwesomeNotifications().getLocalTimeZoneIdentifier();
  var schedule = NotificationCalendar(
    year: DateTime.now().year,
    month: DateTime.now().month,
    day: DateTime.now().day,
    hour: 11, // 3 PM in 24-hour format
    minute: 33,
    second: 0,
    millisecond: 0,
    allowWhileIdle: true,
  );
  return AwesomeNotifications().createNotification(
      // schedule: schedule,
      actionButtons: [
        NotificationActionButton(
          key: 'OPEN',
          label: "Let's go",

        ),
      ],
      content: NotificationContent(
          // payload: '/notification-page',
          id: 10,
          channelKey: 'basic_channel',
          title: 'Xin chào ngày mới',
          body: 'Hãy cập nhật quá trình của bạn thôi',
          wakeUpScreen: true,
          payload: {'route':'/noti-page'}
      )
  );
}
