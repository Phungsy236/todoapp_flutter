import 'package:flutter/material.dart';
import 'package:todo_list/models/Activity.dart';
import 'package:todo_list/store/appState.dart';
import 'package:todo_list/utils/getDirections.dart';
import 'package:todo_list/widgets/Dialog.dart';
import 'package:todo_list/widgets/streakcount.dart';
import 'package:get/get.dart';

class CardTodo extends StatefulWidget {
  final Activity activity;

  const CardTodo({Key? key, required this.activity}) : super(key: key);

  @override
  State<CardTodo> createState() => _CardTodoState();
}
class _CardTodoState extends State<CardTodo> {
  @override
  void initState() {
    //   // TODO: implement initState
    super.initState();
  }

  AppState store = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: getScreenWidthExcludeSafeArea(context),
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
      // color: Colors.blueGrey,
      child: Card(
          elevation: 8,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(24))),
          // shadowColor: Colors.,
          child: Container(
            padding: EdgeInsets.only(bottom: 12 ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: PopupMenuButton(
                        // initialValue: selectedMenu,
                        // Callback that sets the selected popup menu item.
                        onSelected: (item) {
                          if(item == 1){
                            showSimpleModalDialog(context,widget.activity);
                            return;
                          }
                          if(item == 2 ){
                            showDialog(
                                context: context,
                                builder: (context) => Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical:
                                      MediaQuery.of(context).size.height /
                                          4,
                                      horizontal: 42),
                                  child: Card(
                                    child: Container(
                                        padding: EdgeInsets.symmetric(vertical: 50, horizontal: 32),
                                        child: Column(children: [
                                          Text(
                                            "Confirm delete ?",
                                            style: Theme.of(context).textTheme.headlineSmall!.copyWith(
                                                fontWeight: FontWeight.bold
                                            ),
                                          ),
                                          SizedBox(
                                            height: 36,
                                          ),
                                          Text(
                                            "Are you sure want to delete this activity?",
                                            textAlign: TextAlign.center,
                                            style: Theme.of(context).textTheme.bodyMedium
                                            ,
                                          ),
                                          SizedBox(
                                            height: 36,
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              ElevatedButton(
                                                onPressed: () { Navigator.pop(context);},
                                                child: Text("Cancel"),
                                                style:
                                                ElevatedButton.styleFrom(
                                                    backgroundColor:
                                                    Colors.grey),
                                              ),
                                              SizedBox(
                                                width: 24,
                                              ),
                                              ElevatedButton(
                                                  onPressed: () {
                                                    store.removeActivity(widget.activity);
                                                    Navigator.pop(context);
                                                  },
                                                  child: Text("Delete"))

                                            ],
                                          )
                                        ])),
                                  ),
                                ));
                          }
                        },
                        itemBuilder: (BuildContext context) => <PopupMenuEntry>[
                          const PopupMenuItem(
                            value: 1,
                            child: Text('Update'),

                          ),
                          const PopupMenuItem(
                            value: 2,
                            child: Text('Delete'),

                          ),
                        ],
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              Activity a = new Activity.clone(widget.activity);
                              if (a.count >= 1) {
                                a.count -= 1;
                                store.updateActivity(a);
                              }
                            },
                            child: Icon(Icons.exposure_minus_1),
                            style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(20),
                              backgroundColor: Colors.blue, // <-- Button color
                            )),
                        SizedBox(
                          width: 24,
                        ),
                        ElevatedButton(
                            onPressed: () {
                              Activity a = new Activity.clone(widget.activity);
                              a.count += 1;
                              store.updateActivity(a);
                            },
                            child: Icon(Icons.plus_one),
                            style: ElevatedButton.styleFrom(
                              shape: CircleBorder(),
                              padding: EdgeInsets.all(20),
                              backgroundColor: Colors.blue, // <-- Button color
                            )),
                      ],
                    ),
                  ],
                ),

                StreakCount(
                  count: widget.activity.count,
                ),
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Text(
                    widget.activity.name,
                    style: Theme.of(context).textTheme.displaySmall!.copyWith(
                      fontWeight: FontWeight.bold,
                      color: Colors.black87
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
