import 'package:flutter/material.dart';

class StreakCount extends StatelessWidget {
  final int count;
  // final Function() setCount ;
  const StreakCount({super.key, required this.count });

  BoxDecoration renderStreakImg() {
    if (count >= 3 && count < 7) {
      return const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/fire.jpg"),
            fit: BoxFit.cover,
          ));
    } else if (count >= 7) {
      return const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/firev2.jpg"),
            fit: BoxFit.cover,
          ));
    } else {
      return const BoxDecoration(
          image: null
      );
    }
  }
   Color getTextStreakColor(){
    if(count>=3 && count < 7){
      return Colors.deepOrange;
    }else if(count >= 7){
      return Colors.black87;
    }else {
      return Colors.black;
    }
   }
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        child: Container(
            // width: 400,
            // height: 350,
            decoration: renderStreakImg(),
            child: Center(child:  Text(count.toString(),style: TextStyle(
                fontSize: 80, fontWeight: FontWeight.bold, color: getTextStreakColor() )))),
      ),
    );
  }
}

