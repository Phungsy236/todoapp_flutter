import 'package:flutter/material.dart';
import 'package:todo_list/models/Activity.dart';
import 'package:get/get.dart';


import 'package:todo_list/store/appState.dart';

class FormCreateActivity extends StatefulWidget {
  late Activity? initValue;
  FormCreateActivity({Key? key , this.initValue }) : super(key: key );

  @override
  State<FormCreateActivity> createState() => _FormCreateActivity();
}

class _FormCreateActivity extends State<FormCreateActivity> {
  final _formKey = GlobalKey<FormState>();
  late final nameController= TextEditingController(text : widget.initValue?.name);
  late final desController = TextEditingController(text : widget.initValue?.description);


  closeModal(){
      Navigator.pop(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    nameController.dispose();
    desController.dispose();
  }
  AppState store = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(12),
        child: Column(children: [
          Form(
            key: _formKey,
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter activity name';
                      }
                      return null;
                    },
                    controller: nameController,
                    maxLength: 36,
                    decoration: InputDecoration(
                        labelText: 'Activity name',
                        // Set border for enabled state (default)
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 1, color: Colors.blue),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        errorBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 2.5, color: Colors.red),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        // Set border for focused state
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 2.5, color: Colors.blue),
                          borderRadius: BorderRadius.circular(15),
                        )),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  TextFormField(
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please enter activity description';
                      }
                      return null;
                    },
                    controller: desController,
                    keyboardType: TextInputType.text,
                    maxLines: 8,
                    maxLength: 200,
                    decoration: InputDecoration(
                        labelText: 'Activity description',
                        // Set border for enabled state (default)
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 1, color: Colors.blue),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        // Set border for focused state
                        errorBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 2.5, color: Colors.red),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(width: 2.5, color: Colors.blue),
                          borderRadius: BorderRadius.circular(15),
                        )),
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_formKey.currentState!.validate()) {
                        if(widget.initValue != null){
                            widget.initValue?.name = nameController.text;
                            widget.initValue?.description = desController.text;
                            store.updateActivity(widget.initValue!);
                            closeModal();
                        }else{
                        Activity tmp = new Activity.createNew(nameController.text, desController.text);
                        store.addActivity(tmp);
                        nameController.clear();
                        desController.clear();
                        closeModal();
                        // ScaffoldMessenger.of(context).showSnackBar(
                        //   const SnackBar(content: Text('Add Activity Success!!!') , backgroundColor: Colors.green, animation: kAlwaysCompleteAnimation, ),
                        // );
                      }}
                    },
                    child: const Text('Save'),
                  ),
                ],
              ),
            ),
          )
        ]));
  }
}

showSimpleModalDialog(context , [initValue]) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              constraints: BoxConstraints(maxHeight: 550),
              child: Padding(
                padding: const EdgeInsets.all(2.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(alignment: Alignment.topRight,child:IconButton(onPressed: () {Navigator.pop(context);}, icon: Icon(Icons.close) )),
                    Center(child: Text( initValue!= null ? "Update Acivity": "Create Activity" , style: TextStyle(fontWeight: FontWeight.bold , fontSize: 30)),),
                    FormCreateActivity(initValue: initValue)
                  ],
                ),
              ),
            ),
          ),
        );
      });
}
