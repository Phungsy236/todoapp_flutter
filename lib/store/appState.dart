import 'dart:convert';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:todo_list/models/Activity.dart';

const String KEY = 'todoList';

class AppState extends GetxController {
  var openModalCreate = false.obs;
  var openModalUpdate = false.obs;
  RxList<Activity> listActivity = <Activity>[].obs;

  final box = GetStorage();

  loadState() {
    final jsonList = box.read(KEY);
    if (jsonList != null) {
      var test = jsonDecode(jsonList[0]);

      var tmp =
          (jsonList as List<dynamic>).map((json) => Activity.fromJson(jsonDecode(json)));
      listActivity = tmp.toList().obs;
    }
  }

  triggerModalCreate(bool status) {
    openModalCreate = RxBool(status);
  }

  triggerModalUpdate(bool status) {
    openModalUpdate = RxBool(status);
  }

  writeToStorage() {
    final jsonList =
        listActivity.map((obj) => json.encode(obj.toJson())).toList();
    box.write(KEY, jsonList);
  }

  addActivity(Activity act) {
    listActivity.add(act);
    writeToStorage();
  }

  removeActivity(Activity act) {
    listActivity.remove(act);
    print(listActivity);
    writeToStorage();
  }

  updateActivity(Activity act) {
    // var tmp =  listActivity.where((element) => element.id != act.id).toList();
    // tmp.add(act);
    // listActivity = tmp.obs;
    int index = listActivity.indexWhere((element) => element.id == act.id);
    listActivity[index] = act;
    writeToStorage();
  }
}
