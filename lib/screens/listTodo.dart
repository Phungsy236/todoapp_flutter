import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/store/appState.dart';
import 'package:todo_list/utils/sendNotifyCation.dart';
import 'package:todo_list/widgets/Dialog.dart';
import 'package:todo_list/widgets/cardTodo.dart';
import 'package:get/get.dart';

class ListTodo extends StatefulWidget {
  const ListTodo({Key? key}) : super(key: key);

  @override
  State<ListTodo> createState() => _ListTodoState();
}

class _ListTodoState extends State<ListTodo> {
  final AppState store = Get.find();

  @override
  void handleAddActivity() {
    showSimpleModalDialog(context);
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    store.loadState();
  }
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Todo List",
            style: Theme.of(context)
                .textTheme
                .titleLarge!
                .copyWith(fontWeight: FontWeight.bold)),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () {
                handleAddActivity();
              },
              icon: Icon(Icons.add, size: 36))
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        padding: EdgeInsets.symmetric(vertical: 12),
        elevation: 0,
        child: ElevatedButton(
          child: Icon(Icons.add),
          onPressed: () {
            // handleAddActivity();
            callNotify();
            // AwesomeNotifications().createNotification(
            //     content: NotificationContent(
            //     id: 10,
            //     channelKey: 'basic_channel',
            //     title: 'Simple Notification',
            //     body: 'Simple body',
            //     // actionType: ActionType.Default
            // ));
          },
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            padding: EdgeInsets.all(20),
            backgroundColor: Colors.blue, // <-- Button color
          ),
        ),
      ),
      body: Obx(() => ListView(
            scrollDirection: Axis.horizontal,
            children: store.listActivity.length > 0
                ? store.listActivity
                    .map((activity) => CardTodo(activity: activity))
                    .toList()
                : [
                    ConstrainedBox(
                      constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width),
                      child: Align(
                        alignment: Alignment.center,
                        child: Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                "Add new Activity",
                                style: TextStyle(
                                    fontSize: 32,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey),
                              ),

                            ],
                          ),
                        ),
                      ),
                    )
                  ],
          )),
    );
  }
}
