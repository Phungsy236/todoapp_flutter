import 'package:todo_list/utils/getDirections.dart';

class Activity {
  late String id;

  late String name;
  late int count;

  late String description;
  late bool isDeleted;

  Activity.createNew(String name, String description) {
    this.name = name;
    this.description = description;
    count = 0;
    isDeleted = false;
    id = getRandomString(8);
  }

  Activity.clone(Activity cloned) {
    id = cloned.id;
    name = cloned.name;
    description = cloned.description;
    count = cloned.count;
    isDeleted = cloned.isDeleted;
  }

  Activity.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    count = json['count'];
    description = json['description'];
    isDeleted = json['isDeleted'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['count'] = count;
    map['description'] = description;
    map['isDeleted'] = isDeleted;
    return map;
  }
}
